/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package variablesaplicacion;

/**
 *
 * @author israelarias
 */
public class VariablesAplicacion {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //variable de tipo entero
        byte variableByte;
        System.out.println("Valor de Byte");
        System.out.println("Rango maximo: " + Byte.MAX_VALUE);
        System.out.println("Rango minimo: " + Byte.MIN_VALUE);

        short variableShort;
        System.out.println("Valor de Short");
        System.out.println("Rango maximo: " + Short.MAX_VALUE);
        System.out.println("Rango minimo: " + Short.MIN_VALUE);

        int varibleEntero;
        System.out.println("Valor Entero");
        System.out.println("Rango maximo: " + Integer.MAX_VALUE);
        System.out.println("Rango minimo: " + Integer.MIN_VALUE);

        long variableLong;
        System.out.println("Valor long");
        System.out.println("Rango maximo: " + Long.MAX_VALUE);
        System.out.println("Rango minimo: " + Long.MIN_VALUE);

        //variable de tipo decimal
        float variableDecimaluno;
        System.out.println("Valor Float");
        System.out.println("Rango maximo: " + Float.MAX_VALUE);
        System.out.println("Rango minimo: " + Float.MIN_VALUE);

        double variableDecimalDos;
        System.out.println("Valor Double");
        System.out.println("Rango maximo: " + Double.MAX_VALUE);
        System.out.println("Rango minimo: " + Double.MIN_VALUE);

        //variable tipo caracter
        char variablechar = 61;
        System.out.println("variablechar : " + variablechar);

        //Varaible logica
       
        boolean variableLogica = false;
        //System.out.println("variableLogica = " + variableLogica);

        if (variableLogica == true) {
            System.out.println("Es verdadero");
        } else {
            System.out.println("Es Falso");
        }

        int edad = 18;
        //boolean esAdulto = edad >= 18;

        if (edad >= 18) {
            System.out.println("Eres mayor de edad");
        } else {
            System.out.println("Eres menor de edad");
        }

    }

}
